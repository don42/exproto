use std::fs;
use std::io::{Result, Write};
use std::path::Path;
use std::process::{Command, ExitStatus};
use std::time::Instant;

use chrono::prelude::*;
use clap::{App, AppSettings, Arg, ArgMatches};

#[derive(Debug)]
struct CommandMeasurement {
    real: f32,
    status: ExitStatus,
    start_time: DateTime<Utc>,
}

fn time_command(command_name: &str, args: Vec<&str>) -> Result<CommandMeasurement> {
    let now = Instant::now();
    let start: DateTime<Utc> = Utc::now();
    let status = Command::new(command_name).args(&args[..]).status()?;
    let elapsed = now.elapsed();
    Ok(CommandMeasurement {
        real: elapsed.as_secs_f32(),
        status,
        start_time: start,
    })
}

fn write_to_log(path: &Path, measurement: CommandMeasurement) -> i32 {
    let mut file = fs::OpenOptions::new()
        .create(true)
        .append(true)
        .open(path)
        .unwrap();
    let code = measurement.status.code().unwrap();
    writeln!(
        file,
        "{}|{}|{}",
        measurement.start_time, code, measurement.real
    )
    .expect("unable to write log file");
    code
}

fn main_inner(args: &ArgMatches) -> i32 {
    let cmd = args.value_of("command").expect("unable to parse command");
    let cmd_args: Vec<&str> = args
        .values_of("args")
        .expect("unable to parse arguments")
        .collect();
    let log_path = args
        .value_of("log")
        .map(|p| Path::new(p))
        .expect("unable to parse log path");

    match time_command(cmd, cmd_args) {
        Ok(measurement) => {
            println!("Measurement: {:?}", measurement);
            write_to_log(log_path, measurement)
        }
        Err(ref e) if e.kind() == std::io::ErrorKind::NotFound => 127,
        Err(ref e) if e.kind() == std::io::ErrorKind::PermissionDenied => 126,
        Err(ref e) if e.kind() == std::io::ErrorKind::OutOfMemory => 126,
        _ => 125,
    }
}

fn main() {
    let matches = App::new("time")
        .version("0.1.0")
        .author("<don@0xbeef.org>")
        .about("Measure and log execution time and status.")
        .setting(AppSettings::TrailingVarArg)
        .setting(AppSettings::AllowLeadingHyphen)
        .arg(
            Arg::with_name("log")
                .short("l")
                .long("log")
                .takes_value(true)
                .required(true)
                .help("File to append logs to"),
        )
        .arg(
            Arg::with_name("command")
                .long("command")
                .index(1)
                .required(true),
        )
        .arg(Arg::with_name("args").long("args").multiple(true).index(2))
        .get_matches();

    let code = main_inner(&matches);
    std::process::exit(code);
}
